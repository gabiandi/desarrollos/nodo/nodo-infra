# Nodo infra

Infraestructura de software para ***Nodo Brote Nativo***.

## Configuración

En el archivo *.env_example* se encuentra un ejemplo de configuración para todas las variables de entorno relevantes a la ejecución de la infraestructura.

### Base de datos

- ``DATABASE_USER``: configura el nombre de usuario con el cual se iniciara sesión.
- ``DATABASE_PASSWORD``: configura la contraseña con el cual se iniciara sesión.
- ``DATABASE_DB``: nombre de la base de datos con la cual se iniciara la aplicación. ***IMPORTANTE*:** este valor solo se debe de cambiar al iniciar la base de datos. De otra manera, se debe modicar el nombre de la base de datos con una query SQL.

### Tryton

- ``TRYTOND_SECTION_NAME``: configuración relacionada a [Tryton](https://docs.tryton.org/projects/server/en/latest/topics/configuration.html).
- ``TRYTON_PORT``: puerto que expone la interfaz de Tryton.

### API

- ``API_PORT``: puerto que expone la api.
- ``API_SECRET_KEY``: clave secreta para encriptar los token de sesión de la api.

## Inicialización

Para inicializar la infraestructura se debe tener las imagenes del proyecto para los servicios:

- ``nodo-tryton:{VERSION}``
- ``nodo-api:{VESION}``
- ``nodo-web:{VESION}``

Luego se debe de inicializar la base de datos. En esta instancia se pedira la contraseña del usuario *admin*:

~~~bash
source .env
docker compose up -d database
docker compose run --rm tryton trytond-admin -d ${DATABASE_DB} --all -vv --activate-dependencies --email mail@example.com -p -l es
~~~

Lo siguiente que debe hacer es iniciar el servicio de Tryton y configurar el entorno, esto incluye **como mínimo** iniciar el **módulo custom**. De otra manera, la api dará errores y no se iniciará:

~~~bash
docker compose run -e DB_NAME=${DATABASE_DB} --rm tryton trytond_nodo_setup_scenario
~~~

Con esto ya puede iniciar todos los servicios del la infraestructura:

~~~bash
docker compose up -d
~~~

Ahora dispone de una instalación limpia de la infraestructura.

## Detener y borrar

Puede eliminar las instancias y contenedores con el siguiente comando:

~~~bash
docker compose down
~~~

Si quiere tambien eliminar las imagenes y la instalación:

~~~bash
docker images -a | grep "nodo" | awk '{print $3}' | xargs docker rmi
~~~

Para borrar **cache de construcción** ejecute:

~~~bash
docker builder prune -a -f
~~~

Si quiere **eliminar los volumenes** de datos **bajo su propia responsabilidad** debe ejecutar:

~~~bash
docker volume ls | grep "nodo" | awk '{print $2}' | xargs docker volume rm
~~~
